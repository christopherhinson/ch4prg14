//Chris Hinson
//Chapter 4 Program 14
//This class handles outputting the contents of the selected file

import java.io.IOException;
import java.util.Scanner;
import java.io.*;

public class Output {

    public static void printContents(java.io.File fileToRead) throws IOException
    {
        int lineNo = 0;
        Scanner fileIn = new Scanner(fileToRead);

        while (fileIn.hasNext())
        {
            System.out.print(lineNo + ":");
            System.out.println(fileIn.nextLine());
            lineNo++;
        }

        fileIn.close();
    }
}
