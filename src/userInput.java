//Chris Hinson
//Chapter 4 Program 14
//This class handles user Input to get the location of the file

import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class userInput {

    public File getfile()
    {
        Boolean valid = Boolean.FALSE;
        Scanner keyboard = new Scanner(System.in);
        String filepath = null;

        System.out.println("Please enter the path to the textfile you would like to read");

        do
        {
            try
            {
                filepath = keyboard.next();
                File fileToRead = new File(filepath);

                if (fileToRead.exists())
                {
                    System.out.println("Excellent, printing contents of " + filepath);
                    valid = Boolean.TRUE;

                }
                else
                {
                    System.out.println("That file does not exist please try again");
                }

            }
            catch (InputMismatchException e)
            {
                System.out.println("Please enter a string filepath that leads to the file you want to read");
            }
        }
        while(!valid);

        File fileToRead = new File(filepath);

        return fileToRead;
    }
}
